# dcc_adventures

Various adventures designed for Dungeon Crawl Classics tabletop roleplaying game. The author is attributed, but otherwise is owned and written by Joseph Holland.

# Licencing

Unless otherwise noted, permission is hereby granted to any and all that find these modules to use, play with, adapt, or make copy of. However any monetary gain is prohibited unless an agreement is made with the author and owner stated above and upon Gitlab to whom the original work is attributed to.